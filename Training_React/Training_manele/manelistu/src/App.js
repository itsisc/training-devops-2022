import Manele from "./components/Manele";

function App() {
    return (
        <div className="App">
            <Manele />
        </div>
    );
}

export default App;
