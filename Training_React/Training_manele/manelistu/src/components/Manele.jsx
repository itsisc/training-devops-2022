import React from "react";
import "./Manele.css";

import Guta from "../Images/Guta.png";
import Salam from "../Images/Salamior pufos.png";
import Vijelie from "../Images/Vijelie bascalie.png";

function Manele() {
    return (
        <div>
            <div class="backround">
                <div class="video">
                    <iframe
                        width="420"
                        height="315"
                        src="https://www.youtube.com/embed/0wFIgiUzF4o?autoplay=1&muted=0"
                        allow="autoplay"
                        frameborder="0"
                    ></iframe>
                </div>

                <div class="button-container">
                    <a href="https://www.youtube.com/watch?v=tXO2QtjixaM">
                        <img class="img" src={Salam} />
                    </a>
                    <a href="https://www.youtube.com/watch?v=W5hb-9-R1VQ">
                        <img class="img" src={Guta} />
                    </a>
                    <a href="https://www.youtube.com/watch?v=g5fttsJpbZw">
                        <img class="img" src={Vijelie} />
                    </a>
                </div>
            </div>
        </div>
    );
}

export default Manele;
