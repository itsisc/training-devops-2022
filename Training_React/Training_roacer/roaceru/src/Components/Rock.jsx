import React from "react";
import "./Rock.css";

import Queen from "../Images/Queen.png";
import ACDC from "../Images/acdc.png";
import LZ from "../Images/LZ.png";

function Rock() {
    return (
        <div>
            {" "}
            <div class="backround">
                <div class="video">
                    <iframe
                        width="420"
                        height="315"
                        src="https://www.youtube.com/embed/uFC3jY451xI?autoplay=1&muted=0"
                        allow="autoplay"
                        frameborder="0"
                    ></iframe>
                </div>

                <div class="button-container">
                    <a href="https://www.youtube.com/watch?v=fJ9rUzIMcZQ">
                        <img class="img" src={Queen} />
                    </a>
                    <a href="https://www.youtube.com/watch?v=l482T0yNkeo">
                        <img class="img" src={ACDC} />
                    </a>
                    <a href="https://www.youtube.com/watch?v=QkF3oxziUI4">
                        <img class="img" src={LZ} />
                    </a>
                </div>
            </div>
        </div>
    );
}

export default Rock;
