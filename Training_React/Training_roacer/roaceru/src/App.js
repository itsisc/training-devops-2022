import Rock from "./Components/Rock";

function App() {
    return (
        <div className="App">
            <Rock />
        </div>
    );
}

export default App;
